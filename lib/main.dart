import 'package:flutter/material.dart';
import 'package:prroject_test/auto_route/route.gr.dart';
import 'presentation/home/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final _router = AppRouter();
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      // routerDelegate: AutoRouterDelegeate(_router),
      routerDelegate: _router.delegate(),
      routeInformationParser: _router.defaultRouteParser(),
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
    );
  }

  // ignore: non_constant_identifier_names
}
