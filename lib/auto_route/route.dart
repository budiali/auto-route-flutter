import 'package:auto_route/annotations.dart';
import 'package:prroject_test/presentation/dashboard/dashboard.dart';
import 'package:prroject_test/presentation/home/home.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Route,Route',
  routes: <AutoRoute>[
    AutoRoute(page: Dashboard),
    AutoRoute(page: Home, initial: true),
  ],
)
// extend the generated private router
class $AppRouter {}
