import 'package:flutter/material.dart';
import 'package:prroject_test/auto_route/route.gr.dart' as route_gr;
import 'package:prroject_test/widgets/widgets_with_label.dart';
import 'package:prroject_test/presentation/dashboard/dashboard.dart';
import 'package:auto_route/auto_route.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: const Text('Course Online'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ElevatedButton(
              onPressed: () {
                // AutoRouter.of(context).pushNamed('/Dashboard');
                context.router
                    .push(route_gr.Dashboard(textApp: 'Hal Dashboard'));
              },
              child: const Text('Go to Dashboard')),
          Container(
            height: 80.0,
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: const [
                IconWithLabel(
                  icon: Icons.call,
                  text: 'Call',
                  iconColor: Colors.green,
                  textColor: Colors.blue,
                ),
                IconWithLabel(
                  icon: Icons.location_city,
                  text: 'Location',
                  iconColor: Colors.green,
                  textColor: Colors.blue,
                ),
                IconWithLabel(
                  icon: Icons.share,
                  text: 'Share',
                  iconColor: Colors.green,
                  textColor: Colors.blue,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
