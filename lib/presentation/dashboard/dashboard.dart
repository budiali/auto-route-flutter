import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:prroject_test/auto_route/route.gr.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key, @required this.textApp}) : super(key: key);
  final String? textApp;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(textApp!),
      ),
      body: ElevatedButton(
          onPressed: () {
            context.router.pop(true);
          },
          child: const Text('Go Back Previous')),
    );
  }
}
